import arcade.key
from random import randint, random


class Model:
    def __init__(self, world, x, y,):
        self.world = world
        self.x = x
        self.y = y

class bomb(Model):
    def __init__(self, world, x, y):
        super().__init__(world, x, y)
        self.world = world

class treasure(Model):
    def __init__(self, world, x, y):
        super().__init__(world, x, y)
        self.world = world


class Jelly(Model):
    def __init__(self, world, x, y):
        super().__init__(world, x, y)
        self.world = world
        self.state = 0

    def check_state(self,number):
        if(number % 8 == 0):
            self.state = 1
        elif(number % 8 == 1):
            self.state = 2
        elif(number % 8 == 2):
            self.state = 3
        elif(number % 8 == 3):
            self.state = 4
        elif(number % 8 == 4):
            self.state = 5
        elif(number % 8 == 5):
            self.state = 6
        elif(number % 8 == 6):
            self.state = 7
        elif(number % 8 == 7):
            self.state = 8
        return self.state

    def random_location(self):
        self.x = randint(60, self.world.width - 60)
        self.y = randint(120, self.world.height - 120)

    def check_left(self):
        if(self.state == 1 or self.state == 2):
            return True
        return False

    def check_right(self):
        if(self.state == 3 or self.state == 4):
            return True
        return False

    def check_up(self):
        if(self.state == 5 or self.state == 6):
            return True
        return False

    def check_down(self):
        if(self.state == 7 or self.state == 8):
            return True
        return False

    def check_treasure1(self):
        if(self.state == 9):
            return True
        return False

    def check_treasure2(self):
        if(self.state == 10):
            return True
        return False


class PressListenner :
    def __init__(self) :
        self.__handler = []

    def add(self, handler) :
        self.__handler.append(handler)

    def notify(self, *args, **keywargs) :
        for handler in self.__handler :
            handler(*args, **keywargs)

class World:

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.jelly = Jelly(self, 400, 400)
        self.treasure1 = treasure(self, 400, 350)
        self.treasure2 = treasure(self, 400, 350)
        self.bomb = bomb(self, 400, 350)
        self.press_listenners = PressListenner()

        self.score = 0
        self.score2 = 0
        self.gold = 0
        self.gold2 = 0
        self.statebomb = 0



    def on_key_press(self, key, key_modifiers):
        if(self.jelly.state != 9 and self.jelly.state !=10):
            if key == arcade.key.LEFT:
                if self.jelly.check_left():
                    self.press_listenners.notify()
                    self.score2 += 1
                else:
                    self.score2 -= 0.5
            elif key == arcade.key.RIGHT:
                if self.jelly.check_right():
                    self.press_listenners.notify()
                    self.score2 += 1
                else:
                    self.score2 -= 0.5
            elif key == arcade.key.UP:
                if self.jelly.check_up():
                    self.press_listenners.notify()
                    self.score2 += 1
                else:
                    self.score2 -= 0.5
            elif key == arcade.key.DOWN:
                if self.jelly.check_down():
                    self.press_listenners.notify()
                    self.score2 += 1
                else:
                    self.score2 -= 0.5


            elif key == arcade.key.A:
                if self.jelly.check_left():
                    self.press_listenners.notify()
                    self.score += 1
                else:
                    self.score -= 0.5
            elif key == arcade.key.D:
                if self.jelly.check_right():
                    self.press_listenners.notify()
                    self.score += 1
                else:
                    self.score -= 0.5
            elif key == arcade.key.W:
                if self.jelly.check_up():
                    self.press_listenners.notify()
                    self.score += 1
                else:
                    self.score -= 0.5
            elif key == arcade.key.S:
                if self.jelly.check_down():
                    self.press_listenners.notify()
                    self.score += 1
                else:
                    self.score -= 0.5

        else:
            if key == 49:
                if self.jelly.check_treasure1():
                    self.score = 0
                    self.score2 = 0
                    self.gold += 1
                    self.press_listenners.notify()
            elif key == 50:
                if self.jelly.check_treasure1() or self.jelly.check_treasure2():
                    self.score = 0
                    self.score2 = 0
                    self.gold += 0
                    self.statebomb = 1 #bomb
                    #self.press_listenners.notify()
            elif key == 57:
                if self.jelly.check_treasure2():
                    self.score = 0
                    self.score2 = 0
                    self.gold2 += 1
                    self.press_listenners.notify()
            elif key == 48:
                if self.jelly.check_treasure1() or self.jelly.check_treasure2():
                    self.score = 0
                    self.score2 = 0
                    self.gold2 += 0
                    self.statebomb= 1  #bomb
                    #self.press_listenners.notify()

import arcade
import arcade.key
from random import randint, random
from models import World, Jelly

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 700

class ModelSprite(arcade.Sprite):
    def __init__(self, *args, **kwargs):
        self.model = kwargs.pop('model', None)
        super().__init__(*args, **kwargs)

    def sync_with_model(self):
        if self.model:
            self.set_position(self.model.x, self.model.y)

    def draw(self):
        self.sync_with_model()
        super().draw()

class SpaceGameWindow(arcade.Window):
    def __init__(self, width, height):
        super().__init__(width, height)
        arcade.set_background_color(arcade.color.BLACK)
        self.sky_sprite = arcade.Sprite('images/sky.jpg')
        self.sky_sprite .set_position(SCREEN_WIDTH/2,SCREEN_HEIGHT/2)
        self.world = World(width, height)
        self.jelly_sprite = None;
        self.random_image()
        self.treasure1_sprite = ModelSprite('images/treasure1.png',model=self.world.treasure1)
        self.treasure2_sprite =  ModelSprite('images/treasure2.png',model=self.world.treasure2)
        self.bomb_sprite = ModelSprite('images/bomb.png',model=self.world.bomb)
        self.world.press_listenners.add(self.press_listenner_notified)


    def press_listenner_notified(self) :
        self.world.jelly.random_location()
        self.random_image()
        self.world.statebomb = 0


    def random_image(self):
        number = randint(1,800)
        if(self.world.jelly.check_state(number) == 1 ):
            self.jelly_sprite = ModelSprite('images/orleft.png',model=self.world.jelly)
        elif(self.world.jelly.check_state(number) == 2):
            self.jelly_sprite = ModelSprite('images/redright.png',model=self.world.jelly)
        elif(self.world.jelly.check_state(number) == 3):
            self.jelly_sprite = ModelSprite('images/orright.png',model=self.world.jelly)
        elif(self.world.jelly.check_state(number) == 4):
            self.jelly_sprite = ModelSprite('images/redleft.png',model=self.world.jelly)
        elif(self.world.jelly.check_state(number) == 5):
            self.jelly_sprite = ModelSprite('images/orup.png',model=self.world.jelly)
        elif(self.world.jelly.check_state(number) == 6):
            self.jelly_sprite = ModelSprite('images/reddown.png',model=self.world.jelly)
        elif(self.world.jelly.check_state(number) == 7):
            self.jelly_sprite = ModelSprite('images/ordown.png',model=self.world.jelly)
        elif(self.world.jelly.check_state(number) == 8):
            self.jelly_sprite = ModelSprite('images/redup.png',model=self.world.jelly)

    def on_draw(self):
        arcade.start_render()
        if(self.world.gold == 3):
            arcade.set_background_color(arcade.color.BLACK)
            arcade.draw_text("player 1 win",
                    250, self.height/2,
                    arcade.color.RED, 50)
        elif(self.world.gold2 == 3):
            arcade.set_background_color(arcade.color.BLACK)
            arcade.draw_text("player 2 win",
                    250, self.height/2,
                    arcade.color.RED, 50)
        else:
            self.sky_sprite.draw()
            if self.jelly_sprite != None :
                arcade.draw_text('score '+str(self.world.score2),
                        self.width - 140, self.height - 30,
                        arcade.color.RED, 20)

                arcade.draw_text('score '+str(self.world.score),
                            60, self.height - 30,
                            arcade.color.BLUE, 20)
                if(self.world.score < 6 and self.world.score2 < 6):
                    self.jelly_sprite.draw()
            arcade.draw_text('gold  '+str(self.world.gold),
                         60, self.height - 60,
                         arcade.color.BLUE, 20)
            arcade.draw_text('gold  '+str(self.world.gold2),
                         self.width - 140, self.height - 60,
                         arcade.color.RED, 20)
            arcade.draw_text('1 for recieve',220,
                        self.height-30,arcade.color.BLUE,20)
            arcade.draw_text('2 for attack',220,
                        self.height-60,arcade.color.BLUE,20)
            arcade.draw_text('9 for recieve',440,
                        self.height-30,arcade.color.RED,20)
            arcade.draw_text('0 for attack',440,
                        self.height-60,arcade.color.RED,20)

            if(self.world.score >= 6):
                self.treasure1_sprite.draw();
                self.world.jelly.state = 9
            elif(self.world.score2 >= 6):
                self.treasure2_sprite.draw();
                self.world.jelly.state = 10
            elif(self.world.statebomb == 1):
                for i in range(1,20):
                    self.bomb_sprite.draw()
                self.world.press_listenners.notify()
                arcade.pause(0.5)


    def on_key_press(self, key, key_modifiers):
        self.world.on_key_press(key, key_modifiers)



if __name__ == '__main__':
    window = SpaceGameWindow(SCREEN_WIDTH, SCREEN_HEIGHT)
    arcade.run()
